from turtle import circle
from graphics import *
ventana=GraphWin("Simulador movimiento semiparabólico",800,500)
ventana.setCoords(0,0,800,300)

g=9.8
t=0
vi=380
y=85    
while y>=0:
    x=vi*t
    y=y+(-1/2*g*t*t)
    print(x,y)
    circulo=Circle(Point(x,y), 2)
    circulo.setFill('yellow')
    circulo.draw(ventana)
    t=t+0.1

ventana.getMouse()
ventana.close()